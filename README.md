# pngcrush.com #

Online PNG Crush. Free! Provides an API. Simple Quick and Fast.

## Author ##

Written by [Andrew Chilton](https://chilts.org/) for [AppsAttic](https://appsattic.com/) as part of the
[WebDev.sh](https://webdev.sh/) set of tools.

## Links ##

* http://www.worldwidewhat.net/2011/06/html5-native-drag-and-drop-file-upload/
* http://www.sitepoint.com/html5-file-drag-and-drop/
* http://www.w3.org/TR/FileAPI/
* http://www.w3.org/TR/html5/editing.html#dnd

(Ends)
