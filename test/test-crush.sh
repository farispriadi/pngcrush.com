#!/bin/bash
## ----------------------------------------------------------------------------

set -e

# firstly, do a post to local and save the redirect
echo "Posting image to crusher ..."
curl                                                       \
    -X POST                                                \
    -s                                                     \
    --form "input=@awssum-in.png;type=image/png"           \
    https://pngcrush.com/crush > awssum-new.png
echo

# diff with what we expect
echo "Diffing image with what is expected ..."
diff awssum-out.png awssum-new.png
echo

echo "OK"

rm -f redirect.txt awssum-new.png

## ----------------------------------------------------------------------------
