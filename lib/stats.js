// --------------------------------------------------------------------------------------------------------------------
//
// Copyright (c) 2012-2013 AppsAttic Ltd - http://appsattic.com/
// Copyright (c) 2013 Andrew Chilton - http://chilts.org/
//
// --------------------------------------------------------------------------------------------------------------------

// npm
var redis = require('redis')
var rustle = require('rustle')

// --------------------------------------------------------------------------------------------------------------------

// redis
var client = redis.createClient()

var stats = {}

var pages = [ 'home', 'examples', 'crush', 'download' ];
pages.forEach((name) => {
  stats[name] = rustle({
    client       : client,
    domain       : 'pngcrush',      // \
    category     : 'hits',          //  >- Keys: "<domain>:<category>:<name>"
    name         : name,            // /
    period       : 24 * 60 * 60,    // one day
    aggregation  : 'sum',
  })
})

stats.pages = pages

// --------------------------------------------------------------------------------------------------------------------

module.exports = stats

// --------------------------------------------------------------------------------------------------------------------
