// --------------------------------------------------------------------------------------------------------------------

"use strict"

// core
const fs = require('fs')
const path = require('path')

// npm
const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const multer  = require('multer')
const compress = require('compression')
const favicon = require('serve-favicon')
const errorHandler = require('errorhandler')
const PngCrush = require('pngcrush')
const yid = require('yid')
const LogFmtr = require('logfmtr')

// local
const pkg = require('../package.json')
const stats = require('./stats.js')

// --------------------------------------------------------------------------------------------------------------------
// setup

const log = new LogFmtr()

const isProd = process.env.NODE_ENV === 'prod' || process.env.NODE_ENV === 'production'

const upload = multer({
  dest : '/tmp/pngcrush.com',
  limits : {
    fileSize : 5 * 1024 * 1024, // 5MB
  },
})

// --------------------------------------------------------------------------------------------------------------------
// app

var app = express()

app.set('views', __dirname + '/../views')
app.set('view engine', 'pug')
app.enable('trust proxy')

app.locals.pkg = pkg
app.locals.env = process.env.NODE_ENV
app.locals.min = isProd ? '.min' : ''
app.locals.pretty = isProd

// static routes
app.use(compress())
app.use(favicon(__dirname + '/../public/favicon.ico'))

if ( isProd ) {
  var oneMonth = 30 * 24 * 60 * 60 * 1000
  app.use(express.static(__dirname + '/../public/', { maxAge : oneMonth }))
}
else {
  app.use(express.static(__dirname + '/../public/'))
}

app.use(morgan(isProd ? 'combined' : 'dev'))
app.use(upload.single('input'))

// --------------------------------------------------------------------------------------------------------------------
// middleware

app.use((req, res, next) => {
  // add a Request ID
  req._rid = yid()

  // create a RequestID and set it on the `req.log`
  req.log = log.withFields({ rid: req._rid })

  next()
})

app.use(LogFmtr.middleware)

app.use((req, res, next) => {
  // set a `X-Made-By` header :)
  res.setHeader('X-Made-By', 'Andrew Chilton - https://chilts.org - @andychilton')

  // From: http://blog.netdna.com/opensource/bootstrapcdn/accept-encoding-its-vary-important/
  res.setHeader('Vary', 'Accept-Encoding')

  res.locals.title  = false

  // add the advert
  res.locals.ad = {
    title : 'Digital Ocean',
    url   : 'https://www.digitalocean.com/?refcode=c151de638f83',
    src   : 'https://s18.postimg.org/zbi91biah/digital-ocean-728x90.jpg',
    text1 : 'We recommend ',
    text2 : ' for hosting your sites. Free $10 credit when you sign up.',
  }

  next()
})

// --------------------------------------------------------------------------------------------------------------------
// routes

app.get(
    '/',
    (req, res) => {
        req.log.info('/ : Rendering index')
        stats.home.inc()
        res.render('index', { title: 'PNG Crush' })
    }
)

app.get(
    '/examples',
    (req, res) => {
        req.log.info('/examples : entry')
        stats.examples.inc()
        res.render('examples', { title: 'Language Examples for PNG Crush' })
    }
)

// from a post, outputs the text and the text only (with a download header)
app.post('/crush', (req, res) => {
    req.log.info('/crush : crushing input')

    // if there is no files at all, tell the user they need to upload one
    if ( !req.file ) {
        req.log.warn('No req.file')
        res.setHeader('Content-Type', 'text/plain')
        return res.status(500).send('Provide an input PNG image.')
    }

    // remember some things
    var origSize = req.file.size
    if ( origSize === 0 ) {
        req.log.warn('req.file is empty')
        res.setHeader('Content-Type', 'text/plain')
        return res.status(500).send('Input image is of zero length.')
    }

    // ok, looks like we have a file
    req.log.info('opening readStream ' + req.file.path + '...')
    var readStream = fs.createReadStream(req.file.path)

    readStream.on('open', (fd) => {
        req.log.info('readStream opened ' + req.file.path + ', fd=' + fd)
    })

    var m
    var extension
    if ( m = req.file.originalname.match(/\.(\w+)$/) ) {
        extension = m[1]
    }
    var outfile = '/var/lib/' + pkg.name + '/' + req._rid + '.png'
    var writeStream = fs.createWriteStream(outfile)

    writeStream.on('open', (fd) => {
        req.log.info('file opened, fd=' + fd)

        // From: http://zoompf.com/blog/2014/11/png-optimization
        // Forget '-brute' since it takes too long.
        //
        // Try this instead:
        //
        //     $ pngcrush -rem alla -nofilecheck -reduce -m 7 source.png crushed.png
        //
        var args = ['-rem', 'alla', '-nofilecheck', '-reduce', '-m', '7']

        // pipe the readStream -> pngcrush -> writeStream
        var pngcrush = new PngCrush(args)
        var hi = readStream.pipe(pngcrush).pipe(writeStream)
        pngcrush.on('error', (err) => {
            // ignore 'err' since it contains a log of gumph
            req.log.info('Error from PngCrush: ' + req.file.path + ' -> ' + outfile)
            if ( req.xhr ) {
                var data = {
                    ok  : false,
                    msg : 'PngCrush failed, presumably this is not a PNG.',
                }
                res.json(data)
            }
            else {
                res.setHeader('Content-Type', 'text/plain')
                res.status(500).send('PngCrush failed, presumably this is not a PNG.')
            }
        })

        // when the write has finished, tell the user
        writeStream.on('finish', () => {
            req.log.info('writeStream ' + req.file.path + ' finished')

            stats.crush.inc()

            // now, figure out the size of the new file
            fs.stat(outfile, (err, stat) => {
                req.log.info('new file statted')

                if ( req.xhr ) {
                    var data = {
                        ok       : true,
                        input    : req.file.name,
                        output   : '/out/' + req._rid + '.png',
                        origSize : origSize,
                        newSize  : stat.size,
                    }
                    res.json(data)
                }
                else {
                    // a regular post from a form or command line
                    res.download(outfile, 'out.png', (err) => {
                        if (err) {
                            req.log.error('Something went wrong when calling res.download(): ' + err)
                            return next(err)
                        }
                        req.log.info('res.sendFile(): complete ' + req.file.path)
                    })
                }
            })
        })
    })
})

app.get('/out/:filename', (req, res, next) => {
    req.log.info('/out/* - entry')
    var outfile = '/var/lib/' + pkg.name + '/' + req.params.filename
    req.log.info('Downloading ' + outfile)
    res.download(outfile, 'out.png', (err) => {
        if (err) {
            req.log.error('Something went wrong when calling res.download(): ' + err)
            return next(err)
        }
        stats.download.inc()
        req.log.info('res.download(): complete')
    })
})

app.get(
    '/stats',
    (req, res, next) => {
        var finished = false
        var got = 0
        var currentStats = {}

        // get some bits
        stats.pages.forEach((hitName) => {
            stats[hitName].values((err, data) => {
                if ( finished ) return
                if (err) {
                    finished = true
                    return next(err)
                }

                got += 1

                // save this hit
                data.forEach((hit) => {
                    currentStats[hit.ts] = currentStats[hit.ts] || {}
                    currentStats[hit.ts][hitName] = hit.val
                })

                // if we've got all the results, render the page
                if ( got === stats.pages.length ) {
                    finished = true
                    res.render('stats', { stats : currentStats, title : 'stats' })
                }
            })
        })
    }
)

// create the sitemap with all pages
var baseUrl = 'https://pngcrush.com'
var sitemap = [
    `${baseUrl}/`,
    `${baseUrl}/examples`,
]
var sitemapTxt = sitemap.join('\n') + '\n'

app.get(
    '/sitemap.txt',
    (req, res) => {
        res.setHeader('Content-Type', 'text/plain')
        res.send(sitemapTxt)
    }
)

app.use((err, req, res, next) => {
    // connect.multipart() says the file is too big
    if (err.status === 413 ) {
        if ( req.xhr ) {
            var data = {
                ok  : false,
                msg : 'File is too big, should be less than 5MB.',
            }
            return res.json(data)
        }
        else {
            res.setHeader('Content-Type', 'text/plain')
            return res.status(413).send('File is too big, should be less than 5MB.')
        }
    }
    next(err)
})

// error handlers
if ( !isProd ) {
    app.use(errorHandler({ dumpExceptions: true, showStack: true }))
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = app

// --------------------------------------------------------------------------------------------------------------------
