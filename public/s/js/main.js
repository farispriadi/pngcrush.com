const app = new Vue({

  el: '#app',

  data: {
    id: 0,
    filename: '',
    size: 0,
    type: '',
    modified: '',
    preview: "https://via.placeholder.com/600x240?text=Image+Preview",
    images: [],
    err: '',
  },

  methods: {

    onChangeFile() {
      console.log('onChangeFile()')

      // get this input
      var file = this.$refs.files.files[0]

      this.filename = file.name
      this.size = file.size
      this.type = file.type
      this.modified = file.lastModified

      var reader = new FileReader()

      reader.onload = (e) => {
        // set the preview once loaded
        this.preview = e.target.result
      }

      // read the image file as a data URL.
      if ( file.type !== 'image/jpeg' && file.type !== 'image/png' ) {
        this.err = 'Please choose an image file.'
        return
      }

      // start reading the data from the file
      reader.readAsDataURL(file)
    },

    onCrush() {
      console.log('Crush')

      // add a new row to the $uploads table
      const image = {
        // id: thisId,
        filename: this.filename,
        size: this.size,
        newSize: '?',
        saved: '?',
        status: '0%',
      }
      this.images.unshift(image)
      const index = this.images.length - 1

      // Info: http://caniuse.com/xhr2
      var xhr = new XMLHttpRequest()

      // make a new FormData from the form itself
      var fd = new FormData(document.getElementById('input-form'))

      xhr.upload.addEventListener(
        "progress",
        ev => {
          console.log('progress')
          if (ev.lengthComputable) {
            var percentComplete = Math.round(ev.loaded * 100 / ev.total)
            console.log('percentComplete=' + percentComplete)
            image.status = percentComplete + '%'
          }
          else {
            this.err = 'Error when computing progress'
          }
        },
        false
      )
      xhr.addEventListener(
        "load",
        ev => {
          // This event is raised when the server sent back a response
          var data = JSON.parse(ev.target.responseText)

          if ( data.ok ) {
            image.status = 'File crushed.'
            image.newSize = data.newSize
            image.saved = image.size - data.newSize
            image.downloadUrl = data.output
          }
          else {
            image.status = data.msg
          }
        },
        false
      )
      xhr.addEventListener("error", ev => this.err = 'There was an error attempting to upload the file.', false)
      xhr.addEventListener("abort", ev => this.err = 'The upload has been aborted by the user or the browser dropped the connection.', false)

      // and finally, POST to /crush
      xhr.open("POST", "/crush")

      // like jQuery, we should set this so Express knows and can tell us it's an XHR
      xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest")

      // ... and send it
      xhr.send(fd)
    },

    onClear() {
      console.log('Clear')
      this.images = []
    },

  },

})
